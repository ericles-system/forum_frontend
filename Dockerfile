FROM nginx:1.17.4-alpine

RUN apk add npm

WORKDIR /app

COPY . .

RUN npm install 
RUN npm run build
RUN cp -r build/* /usr/share/nginx/html

ENV FORUM_API_HOST "forum-api-container"
ENV PORT "80"

RUN echo "server { \
    listen       ${PORT}; \
    server_name  localhost; \
    \
    location / { \
        root   /usr/share/nginx/html; \
        index  index.html index.htm; \
    } \
    \
    # redirect server error pages to the static page /50x.html \
    # \
    error_page   500 502 503 504  /50x.html; \
    location = /50x.html { \
        root   /usr/share/nginx/html; \
    } \
    \
    # Reverse proxy to app backend \
    # \
    location /api { \
        proxy_pass   http://${FORUM_API_HOST}; \
    } \
}" > /etc/nginx/conf.d/default.conf

CMD [ "nginx", "-g", "daemon off;" ]