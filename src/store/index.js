import { createStore, applyMiddleware } from 'redux';
import { Reducers } from '../reducers';
import thunk from 'redux-thunk';
import promise from 'redux-promise'
import multi from 'redux-multi'
import { composeWithDevTools } from 'redux-devtools-extension';

export const Store = createStore(Reducers, composeWithDevTools(
    applyMiddleware(thunk, promise, multi),
    // other store enhancers if any
));