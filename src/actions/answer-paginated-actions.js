import axios from 'axios';

export const FETCH_ANSWER_BEGIN   = 'FETCH_ANSWER_BEGIN';
export const FETCH_ANSWER_SUCCESS = 'FETCH_ANSWER_SUCCESS';
export const FETCH_ANSWER_FAILURE = 'FETCH_ANSWER_FAILURE';
export const FETCH_ANSWER_RESET = 'FETCH_ANSWER_RESET';

const fetchAnswerBegin = () => ({
  type: FETCH_ANSWER_BEGIN
});

const fetchAnswerSuccess = answers => ({
  type: FETCH_ANSWER_SUCCESS,
  payload: answers
});

const fetchAnswerFailure = error => ({
  type: FETCH_ANSWER_FAILURE,
  payload: { error }
});

export const resetAnswer = () => ({
  type: FETCH_ANSWER_RESET
});

export function fetchAnswerPaginated(questionId, page = 0, limit = process.env.REACT_APP_GRID_RECORDS_PER_PAGE, search) {
  return dispatch => {
    dispatch(fetchAnswerBegin());
    return axios.get(`${process.env.REACT_APP_ANSWERS_URL.replace(':questionId', questionId)}`, 
        {params: Object.assign({ page_size: limit, page: page }, search)}
    ).then(response => {
        dispatch(fetchAnswerSuccess(response.data));
        return response.data;
      })
      .catch(error => dispatch(fetchAnswerFailure(error)));
  };
}
