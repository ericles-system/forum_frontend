import axios from 'axios';

export const FETCH_QUESTION_ADD_BEGIN   = 'FETCH_QUESTION_ADD_BEGIN';
export const FETCH_QUESTION_ADD_SUCCESS = 'FETCH_QUESTION_ADD_SUCCESS';
export const FETCH_QUESTION_ADD_FAILURE = 'FETCH_QUESTION_ADD_FAILURE';
export const FETCH_QUESTION_ADD_RESET = 'FETFETCH_QUESTION_ADD_RESET';

const fetchQuestionAddBegin = () => ({
  type: FETCH_QUESTION_ADD_BEGIN
});

const fetchQuestionAddSuccess = question => ({
  type: FETCH_QUESTION_ADD_SUCCESS,
  payload: { question }
});

const fetchQuestionAddFailure = error => ({
  type: FETCH_QUESTION_ADD_FAILURE,
  payload: { error }
});

export const resetQuestion = () => ({
  type: FETCH_QUESTION_ADD_RESET
});

export function fetchQuestionAdd(obj) {
  return dispatch => {
    dispatch(fetchQuestionAddBegin());
    return axios.post(`${process.env.REACT_APP_QUESTIONS_URL}`, obj)
    .then(response => {
        dispatch(fetchQuestionAddSuccess(response.data));
        return response.data;
      })
      .catch(error => dispatch(fetchQuestionAddFailure(error)));
  };
}
