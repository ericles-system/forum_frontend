import axios from 'axios';

export const FETCH_QUESTION_BEGIN   = 'FETCH_QUESTION_BEGIN';
export const FETCH_QUESTION_SUCCESS = 'FETCH_QUESTION_SUCCESS';
export const FETCH_QUESTION_FAILURE = 'FETCH_QUESTION_FAILURE';
export const FETCH_QUESTION_RESET = 'FETFETCH_QUESTION_RESET';

const fetchQuestion = () => ({
  type: FETCH_QUESTION_BEGIN
});

const fetchQuestionSuccess = questions => ({
  type: FETCH_QUESTION_SUCCESS,
  payload: { questions }
});

const fetchQuestionFailure = error => ({
  type: FETCH_QUESTION_FAILURE,
  payload: { error }
});

export const resetQuestion = () => ({
  type: FETCH_QUESTION_RESET
});

export function fetchQuestionPaginated(page = 0, limit = process.env.REACT_APP_GRID_RECORDS_PER_PAGE, search) {
  return dispatch => {
    dispatch(fetchQuestion());
    return axios.get(`${process.env.REACT_APP_QUESTIONS_URL}`, 
        {params: Object.assign({ page_size: limit, page: page }, search)}
    ).then(response => {
        dispatch(fetchQuestionSuccess(response.data));
        return response.data;
      })
      .catch(error => dispatch(fetchQuestionFailure(error)));
  };
}
