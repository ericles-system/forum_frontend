import axios from 'axios';

export const FETCH_TAGS_BEGIN   = 'FETCH_TAGS_BEGIN';
export const FETCH_TAGS_SUCCESS = 'FETCH_TAGS_SUCCESS';
export const FETCH_TAGS_FAILURE = 'FETCH_TAGS_FAILURE';

const fetchTagsBegin = () => ({
  type: FETCH_TAGS_BEGIN
});

const fetchTagsSuccess = tags => ({
  type: FETCH_TAGS_SUCCESS,
  payload: { tags }
});

const fetchTagsFailure = error => ({
  type: FETCH_TAGS_FAILURE,
  payload: { error }
});

export function fetchTags() {
  return dispatch => {
    dispatch(fetchTagsBegin());
    return axios.get(`${process.env.REACT_APP_TAGS_URL}`)
      .then(response => {
        dispatch(fetchTagsSuccess(response.data));
        return response.data;
      })
      .catch(error => dispatch(fetchTagsFailure(error)));
  };
}