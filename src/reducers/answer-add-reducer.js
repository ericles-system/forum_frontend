import {
    FETCH_ANSWER_ADD_BEGIN,
    FETCH_ANSWER_ADD_SUCCESS,
    FETCH_ANSWER_ADD_FAILURE,
    FETCH_ANSWER_ADD_RESET,
    FETCH_LIKE_QUESTION,
    FETCH_LIKE_ANSWER,
  } from '../actions/answer-add-actions'
  
  const INITIAL_STATE =  {
    error: null,
    loading: false,
    data: [],
    likeQuestions: undefined,
    likeAnswers: undefined
  };
  
  export default function answerAddReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
      case FETCH_ANSWER_ADD_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case FETCH_ANSWER_ADD_SUCCESS:
        return {
          ...state,
          loading: false,
          data: action.payload.question
        };

      case FETCH_LIKE_ANSWER:
        return {
          ...state,
          loading: false,
          likeAnswers: action.payload
        };
  
      case FETCH_LIKE_QUESTION:
        return {
          ...state,
          loading: false,
          likeQuestions: action.payload
        };

      case FETCH_ANSWER_ADD_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          data: undefined
        };

      case FETCH_ANSWER_ADD_RESET:
        return INITIAL_STATE;
  
      default:
        return state;
    }
  }