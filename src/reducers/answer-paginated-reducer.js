import {
    FETCH_ANSWER_BEGIN,
    FETCH_ANSWER_SUCCESS,
    FETCH_ANSWER_FAILURE,
    FETCH_ANSWER_RESET
  } from '../actions/answer-paginated-actions'
  
  const INITIAL_STATE =  {
    error: null,
    loading: false,
    data: {},
    answers: [],
    total: 0
  };
  
  export default function answerPaginatedReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
      case FETCH_ANSWER_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case FETCH_ANSWER_SUCCESS:
        return {
          ...state,
          loading: false,
          answers: action.payload.data.answers,
          data: action.payload.data,
          total: action.payload.total
        };
  
      case FETCH_ANSWER_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          data: undefined
        };

        case FETCH_ANSWER_RESET:
        return FETCH_ANSWER_RESET;
  
      default:
        return state;
    }
  }