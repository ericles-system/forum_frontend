import { combineReducers } from 'redux';
import serviceCounterReducer from './service-counter-reducer';
import QuestionPaginatedReducer from './question-paginated-reducer'
import QuestionAddReducer from './question-add-reducer'
import TagsReducer from './tags-reducer'
import AnswerPaginatedReducer from './answer-paginated-reducer'
import AnsweAddReducer from './answer-add-reducer'

export const Reducers = combineReducers({
  serviceCounter: serviceCounterReducer,
  questionsPaginated: QuestionPaginatedReducer,
  questionsAdd: QuestionAddReducer,
  tags: TagsReducer,
  answerPaginated: AnswerPaginatedReducer,
  answer: AnsweAddReducer
});