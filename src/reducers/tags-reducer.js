import {
    FETCH_TAGS_BEGIN,
    FETCH_TAGS_SUCCESS,
    FETCH_TAGS_FAILURE
  } from '../actions/tags-action';
  
  const initialState = {
    data: undefined,
    loading: false,
    error: null
  };
  
  export default function tagReducer(state = initialState, action) {
    switch(action.type) {
      case FETCH_TAGS_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case FETCH_TAGS_SUCCESS:
        return {
          ...state,
          loading: false,
          data: action.payload.tags.data,
        };
  
      case FETCH_TAGS_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.tags,
          data: undefined,
        };
  
      default:
        return state;
    }
  }