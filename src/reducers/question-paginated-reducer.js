import {
    FETCH_QUESTION_BEGIN,
    FETCH_QUESTION_SUCCESS,
    FETCH_QUESTION_FAILURE,
    FETCH_QUESTION_RESET
  } from '../actions/question-paginated-actions'
  
  const INITIAL_STATE =  {
    error: null,
    loading: false,
    data: [],
    total: 0
  };
  
  export default function questionPaginatedReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
      case FETCH_QUESTION_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case FETCH_QUESTION_SUCCESS:
        return {
          ...state,
          loading: false,
          data: action.payload.questions.data,
          total: action.payload.questions.total
        };
  
      case FETCH_QUESTION_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          data: undefined
        };

        case FETCH_QUESTION_RESET:
        return FETCH_QUESTION_RESET;
  
      default:
        return state;
    }
  }