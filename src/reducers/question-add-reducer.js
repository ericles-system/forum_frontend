import {
    FETCH_QUESTION_ADD_BEGIN,
    FETCH_QUESTION_ADD_SUCCESS,
    FETCH_QUESTION_ADD_FAILURE,
    FETCH_QUESTION_ADD_RESET,
  } from '../actions/question-add-actions'
  
  const INITIAL_STATE =  {
    error: null,
    loading: false,
    data: []
  };
  
  export default function questionAddReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
      case FETCH_QUESTION_ADD_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case FETCH_QUESTION_ADD_SUCCESS:
        return {
          ...state,
          loading: false,
          data: action.payload.question
        };
  
      case FETCH_QUESTION_ADD_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          data: undefined
        };

        case FETCH_QUESTION_ADD_RESET:
        return FETCH_QUESTION_ADD_RESET;
  
      default:
        return state;
    }
  }