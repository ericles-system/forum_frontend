import React, { Component } from "react";
import PropTypes from "prop-types";

import './pagination.css';

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalRecords: "",
      pageLimit: "",
      totalPages: "",
      currentPage: "",
      pagesToShow: ""
    };
  }

  componentDidMount() {
    this.setState({
      totalRecords: this.props.totalRecords,
      pageLimit: this.props.pageLimit || 10,
      totalPages: Math.ceil(this.props.totalRecords / this.props.pageLimit),
      pagesToShow: this.props.pagesToShow || 5,
      currentPage: this.props.page || 1
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      totalRecords: nextProps.totalRecords,
      pageLimit: nextProps.pageLimit || 10,
      totalPages: Math.ceil(nextProps.totalRecords / nextProps.pageLimit),
      pagesToShow: nextProps.pagesToShow || 5,
      currentPage: nextProps.page
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      ( this.state.totalRecords !== prevState.totalRecords ||
      this.state.pageLimit !== prevState.pageLimit ) 
      && this.state.currentPage !== prevState.currentPage
    ) {
      this.setPage(this.state.currentPage);
    }
  }

  setPage(page) {
    var { totalRecords, pageLimit, totalPages } = this.state;

    if (page < 1) {
      page = 1;
    } else if (totalPages > 0 && page > totalPages) {
      page = totalPages;
    }

    this.setState({
      currentPage: page
    });

    var startIndex = (page - 1) * pageLimit;
    var endIndex = Math.min(startIndex + pageLimit - 1, totalRecords - 1);

    this.props.onChangePage({
      pageLimit,
      totalPages,
      page,
      startIndex,
      endIndex
    });
  }

  getPager() {
    var { pagesToShow, currentPage, totalPages } = this.state;
    var pages = [],
      startFromNumber;

    if (totalPages <= pagesToShow) {
      startFromNumber = 1;
      pagesToShow = totalPages;
    } else {
      if (currentPage <= Math.ceil(pagesToShow / 2)) {
        startFromNumber = 1;
      } else if (
        currentPage + Math.floor((pagesToShow - 1) / 2) >=
        totalPages
      ) {
        startFromNumber = totalPages - (pagesToShow - 1);
      } else {
        startFromNumber = currentPage - Math.floor(pagesToShow / 2);
      }
    }

    for (let i = 1; i <= pagesToShow; i++) {
      pages.push(startFromNumber++);
    }

    return {
      currentPage,
      totalPages,
      pages
    };
  }

  render() {    
    if (!this.state.totalRecords || this.state.totalPages === 1) return null;

    var pager = this.getPager();

    return (
      <nav>
        <ul className="d-flex justify-content-center pagination">
          <li className={ pager.currentPage === 1 ? "page-item disabled" : "page-item" }>
            <span 
              className="page-link"
              onClick={() => this.setPage(1)}>
              &#x3C;&#x3C;
            </span>
          </li>
          <li className={ pager.currentPage === 1 ? "page-item disabled" : "page-item" }>
            <span 
              className="page-link"
              onClick={() => this.setPage(pager.currentPage - 1)}>
              &#x3C;
            </span>
          </li>
          {pager.pages.map((page, index) => (
            <li key={index} className={ pager.currentPage === page ? "page-item active" : "page-item" }>
              <span
                className="page-link"
                onClick={() => this.setPage(page)}>
                {page}
              </span>
            </li>
          ))}
          <li className={ pager.currentPage === pager.totalPages ? "page-item disabled" : "page-item" }>
            <span
              className="page-link"
              onClick={() => this.setPage(pager.currentPage + 1)}>
              &#x3E;
            </span>
          </li>
          <li className={ pager.currentPage === pager.totalPages ? "page-item disabled" : "page-item" }>
            <span 
              className="page-link"
              onClick={() => this.setPage(pager.totalPages)}>
              &#x3E;&#x3E;
            </span>
          </li>
        </ul>
      </nav>
    );
  }
}

Pagination.propTypes = {
  totalRecords: PropTypes.number.isRequired,
  pageLimit: PropTypes.number,
  page: PropTypes.number,
  pagesToShow: PropTypes.number,
  onChangePage: PropTypes.func
};

export default Pagination;