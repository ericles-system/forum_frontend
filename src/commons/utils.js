import { store } from 'react-notifications-component';
import * as storage from '../commons/storage/storage';
import React from 'react';

const displayedNotificationsText = [];

/**
 * Default Notification center. Fires notification popup on the screen
 * 
 * @param {*} message The message to be displayed
 * @param {*} type The type of the message. Either one of NotificationType available on utils.js. Defaults to 'success'
 * @param {*} title The title of the notification. Defaults to undefind
 * @param {*} duration The duration of the notification to be displayed. Defaults to 4000 millis
 */
export const addNotification = (message, type = "success", container = "bottom-right", title = undefined, duration = 4000, ) => {
  // check if notification message already exists and avoid to duplicate message on the screen
  if (displayedNotificationsText.find(n => n.message === message) === undefined) {

    const notificationId = store.addNotification({
      title: title,
      message: message,
      type: type,
      content: (
        <div className={ 'notification-'+ type }>
          <div className="notification-content">
            <p className="notification-message">{ message }</p>
          </div>
        </div>),
      insert: "top",
      container: container,
      animationIn: [ "animated", "fadeIn" ],
      animationOut: [ "animated", "fadeOut" ],
      dismiss: {
        duration: duration,
        pauseOnHover: true
      },
      onRemoval: (id, removedBy) => {
        const indexToRemove = displayedNotificationsText.findIndex(n => n.id === id);
        displayedNotificationsText.splice(indexToRemove, 1);
      }
    });
    displayedNotificationsText.push({ id: notificationId, message: message });
  }
};

/**
 * Supported notification types of the addNotification() method
 */
export const NotificationType = {
  SUCCESS: 'success',
  WARNING: 'warning',
  DANGER: 'danger',
  INFO: 'info',
  DEFAULT: 'default'
};

/**
 * Debounces an event for the time 'delay'. 
 * Cancel the previous call if subsequent event is fired before 'delay' millis.
 * 
 * @param {*} func the function to be debounced
 * @param {*} delay delay to fire the event in millis
 */
export const debounceEvent = (func, delay = 400) => {
  let timeoutEvent;
  return (...args) => 
    clearTimeout(timeoutEvent, timeoutEvent = setTimeout(() => func.apply(this, ...args), delay));
};

/**
 * Gets a page from an array, given the page number and records per page.
 * Pagination in memory.
 * 
 * @param {*} arr 
 * @param {*} page 
 * @param {*} recordsPerPage 
 */
export const getPageFromArray = (arr, page = 1, recordsPerPage = 10) => {
  if (!Array.isArray(arr)) throw Error('arr not an array')
  return arr.slice((page - 1) * recordsPerPage, (page * recordsPerPage));
};


export const formatInterval = (interval) => {
  if (interval === null) return 'agora'
  if (interval < 1) return '1 minutos atrás'
  if (interval < 2) return '2 minutos atrás'
  if (interval < 60) return `${Math.floor(interval)} minutos atrás`
  if (interval < 120) return '1 hora'
  if (interval < 1440) return `${Math.floor(interval / 60)} horas atrás`
  if (interval < 2880) return '1 dia atrás'
  return `${Math.floor(interval / 2880)} dias atrás`
}

/**
 * Logout
 */

export function logout() {
	storage.clearAllData();
	window.location.href = '#/';
	// window.location.reload();
}

