import React, { Component } from 'react';
import { connect } from 'react-redux';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './service-counter-overlay.css';

class ServiceCounterOverlay extends Component {
  render() {
    return (
      <div className="overlay" style={{ 'display': this.props.serviceCounter > 0 ? 'flex' : 'none' }}> 
        <FontAwesomeIcon icon={ faSpinner } size="4x" spin inverse  />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  serviceCounter: state.serviceCounter.count
});

export default connect(mapStateToProps)(ServiceCounterOverlay);