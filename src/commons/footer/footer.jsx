import React, { Component } from 'react'
import './footer.css'

export default class Footer extends Component {
  render() {
    return (
      <div className="footer fixed-bottom">
        <div className="copyright">
           &copy; 2020 Forum Br. All Rights Reserved</div>
      </div>
    );
  }
}