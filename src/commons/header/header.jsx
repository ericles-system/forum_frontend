import React, {Component} from 'react';
import './header.css'
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ForumLogo from '../../assets/images/forum-logo.jpg';
import { withRouter, Link } from 'react-router-dom';
import { logout } from '../../commons/utils.js';


class AppHeader extends Component {

  navigateTo(state) {
    this.props.history.push(`/${state}`);
  }

  render() {
    return (
      <div>

        <div className="header">
          <div className="hamburguer-sidebar"> 
            <FontAwesomeIcon icon={ faBars } size='2x' className="nav-icon-purple" data-toggle="modal" data-target="#modal-menu-sidebar" />
          </div>
          <div className="forum-logo-header float-left">
            <Link className="logo-link" to="/home">
              <img src={ ForumLogo } alt="Forum Logo" width="85" />
              <span>Forum</span>
            </Link>
          </div>
          <div className="user-bubble-container float-right" 
            data-toggle="dropdown"
          >
            <span>
              <img alt="Usuário" src={ "https://img.icons8.com/color/64/000000/name.png" } className="user-bubble" />  
            </span>
          </div>
          <div className="dropdown-menu">
            <span className="dropdown-item" onClick={ () => logout() }>Sair</span>
          </div>
          <div 
            style={{ height: '100%' }} 
            className="custom-link client-logo float-right page-header d-flex align-items-center"
            // onClick={ () => this.navigateTo('workspaces') }
            title="Cliente / Projeto">
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(AppHeader);