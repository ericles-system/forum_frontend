import React, { Component } from 'react';
import './forum-select.css';
import Select from 'react-dropdown-select';

export default class ForumSelect extends Component {

  values = [];

  constructor(props) {
    super(props);
    this.values = [];
  }

  dropdownRenderer = ({ props, state, methods }) => {
    
    const filteredOptions = methods.searchResults();
    let options = [];

    if (filteredOptions.length === 0) {
      options.push(
        <span 
          key={ 'dropdown-NO-DATA' }
          className={ `custom-dropdown-item react-dropdown-select-item` } 
          role="option" 
          aria-selected={ false } 
          disabled="disabled"
          color="#0074D9">
          Nenhum resultado encontrado
        </span>);
    } else {
      options = filteredOptions.map((option, i) => {
         return (
           <span 
             className={ `custom-dropdown-item react-dropdown-select-item 
              ${ state.cursor === i ? `react-dropdown-item-active` : '' } 
              ${ methods.isSelected(option) ? 'react-dropdown-select-item-selected' : '' }` } 
             role="option" 
             color="#0074D9"
             aria-label={ option[props.labelField] }             
             tabIndex="-1"
             aria-selected={ methods.isSelected(option) } 
             key={ `dropdown-${ option[props.valueField] }` } 
             onClick={ () => methods.addItem(option) }>
             { option[props.labelField] }
           </span>
         );
      });
    } 

    return (
      <div>
        <div className="dropdown-search-field">
          <input className="form-control" type="text" value={ state.search } onChange={ methods.setSearch } placeholder="Buscar" />
        </div> 
        { options }
      </div>
    );
  };

  contentRenderer = ({ props, state, methods }) => (
    <div className="d-flex col-md-12 no-margin no-padding">
      { 
        props.multi 
          ? (<div className="col-md-11" style={{ marginTop: '4px' }}>{ props.placeholder } ({ state.values.length })</div>)
          : (<div className="col-md-11" style={{ marginTop: '4px' }}>{ state.values.length > 0 ? state.values[0][props.labelField] : props.placeholder }</div>) 
      }
      <span 
        style={ { display: !props.disabled ? 'block': 'none' } } 
        className="react-dropdown-select-clear col-md-1 no-margin no-padding" 
        onClick={ methods.clearAll }>X</span>
    </div>
  );

  /**
   * Control onChange event and avoid to fire the event on component load.
   */
  hasFiredOnLoad = false;
  onChange = (selected) => {
    this.values = [ ...selected ];
    if (this.hasFiredOnLoad && this.props.onChange) {
      this.props.onChange(selected);
    } else {
      this.hasFiredOnLoad = true;
    }
  };

  render() {
    this.values = this.props.values ? [ ...this.props.values ] : [];
    if (this.values && this.values.length > 0 
        && this.props.options && this.props.options.length > 0) {
      let newValues = [];
      this.values.forEach((e) => {
        const selectedValue = this.props.options.find(option => this.props.valueField ? option[this.props.valueField] : option.value === e);
        if ( selectedValue ) newValues.push(selectedValue);
      });
      if (newValues.length > 0) this.values = [ ...newValues ];
    }
    return(
      <Select 
        className="select-forum" 
        { ...this.props }
        options={ [ ...this.props.options ] }
        values={ [ ...this.values ] }
        clearable={ false }
        onChange={ this.onChange }
        dropdownRenderer={ this.dropdownRenderer }
        contentRenderer={ this.contentRenderer }
        />
    );
  }
}