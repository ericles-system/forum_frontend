/**
 * Returns the data associated on the storage with the itemName.
 * 
 * @param {String} itemName 
 * @return {any} value
 */
export function getData(itemName) {
  return JSON.parse(localStorage.getItem(itemName));
}

/**
 * Associates a valid data into fiven nameItem on the storage.
 * 
 * @param {String} nameItem 
 * @param {any} objData 
 */
export function setData(nameItem, objData) {
  const valueStringfied = objData ? JSON.stringify(objData) : undefined;

  if (valueStringfied) {
    localStorage.setItem(nameItem, valueStringfied);
  }
}

/**
 * Clear the storage associated with the nameItem.
 * 
 * @param {String} nameItem 
 */
export function clearData(nameItem) {
  localStorage.removeItem(nameItem);
}

/**
 * Clear all the storage data.
 */
export function clearAllData() {
  localStorage.clear();
};