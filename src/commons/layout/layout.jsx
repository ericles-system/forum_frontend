import React, { Component } from 'react';
import './layout.css';
import Header from '../header/header.jsx';
import Footer from '../footer/footer.jsx';
import Content from '../content/content.jsx';
import QuestionAdd from '../../components/question-add/question-add.jsx'
import Answer from '../../components/answer/answer'
import { Route, Switch } from 'react-router-dom';

export default class Layout extends Component {
  render() {
    return (
      <div className="layout">
        <Header />
        <div>
          <div className="right-container">
            <Content>
              <Switch>
                <Route name="question-add" exact path="/app/pergunta/add" component={ QuestionAdd } />
                <Route name="question-answers" path="/app/pergunta/:questionId/answers" component={ Answer } />
              </Switch>
            </Content>
          </div>
          <Footer />
        </div>
      </div>   
    );
  }
}