import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Layout from './commons/layout/layout';
import Questions from './components/question/question'
import ReactNotification from 'react-notifications-component';
import ServiceCounterOverlay from './commons/service-counter-overlay/service-counter-overlay';
import './App.css';
import './commons/interceptors/service-counter-interceptor';

class App extends Component {

  render() {
    return (
      <div>
        <ServiceCounterOverlay />
        <ReactNotification />
        <Switch>
          <Route name="questions" path="/home" component={ Questions } />  
          <Route path="/app" component={ Layout } />
          <Redirect exact from="/" to="home" />
        </Switch>
      </div>
    );
  }
}

export default App;