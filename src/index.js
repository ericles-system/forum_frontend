import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { Store } from './store';
import { Route, HashRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import 'react-notifications-component/dist/theme.css';

const Root = ({ store }) => (
  <Provider store={ store }>
    <HashRouter>
      <Route path="/" component={ App } />
    </HashRouter>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired
}

ReactDOM.render(<Root store={ Store } />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
