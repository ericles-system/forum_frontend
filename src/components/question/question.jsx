import React, { Component } from 'react'
import { withRouter, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import * as Utils from '../../commons/utils';
import * as QuestionActions from '../../actions/question-paginated-actions'
import { Row, Col, Button } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBroom } from '@fortawesome/free-solid-svg-icons';
import QuestionList from './question-list/question-list'
import Pagination from '../../commons/pagination/pagination'
import Header from '../../commons/header/header'
import Footer from '../../commons/footer/footer'
import './question.css'

class Question extends Component {

    constructor(props) {
        super(props);
		this.inputRef = React.createRef();
    }


	handleAnswers = (question) => {
		this.props.history.push(`/app/pergunta/${question.id}/answers`);
	}

	gridFilter = {
		title: undefined,
		tag_id: undefined
	};

	limit = parseInt(process.env.REACT_APP_GRID_RECORDS_PER_PAGE);
	page = 1

	componentDidMount(){
		this.updateGrid()
		this.onChangePage({ page: this.page })
	}

	/**
	 * Debounces the grid update event to prevent unnecessary service calls
	 */
	debouncedUpdateGrid = Utils.debounceEvent(() => this.updateGrid(), 500);

	/**
	 * Updates the grid information based on the actual page and grid filter options.
	 */
	updateGrid = () => {
		this.props.fetchQuestionPaginated(this.page, this.limit, this.gridFilter);
	};

	/**
  	 * Grid FilterName. Event fired onChange Name option.
	*/
	filterName = (title) => {
		this.page = 1
		this.gridFilter.title = title;
		this.debouncedUpdateGrid();
	};

	/**
  	 * Grid FilterName. Event fired onChange Name option.
	*/
	filterName = (title) => {
		this.page = 1
		this.gridFilter.title = title;
		this.debouncedUpdateGrid();
	};

	onChangePage = (pagination) => {
		// this.toggleSelectAllRows(false);
		this.page = pagination.page
		this.updateGrid();
	  };

	cleanGrid = () => {
		this.gridFilter.title = undefined
		this.inputRef.current.value = null
		this.updateGrid()
	}

  render() {

    return (
		<div>
			<Header />
			<div className="search right-container">
				<Row>
					<Col md={ 8 }>
						<input 
							type="text" 
							placeholder="Pesquisar..." 
							autoFocus 
							autoComplete="off" 
							className="form-control"
							ref={this.inputRef}
							onChange={ (e) => this.filterName(e.target.value) }
						/>
					</Col>
					<Col md={ 1 }>
						<Button className="btn-question btn-shadow" block bsSize="large" type="submit" title="Limpar">
							<FontAwesomeIcon icon={ faBroom } size="lg" onClick={() => this.cleanGrid() }/>
						</Button>
					</Col>
					<Col md={ 3 }>
						<Button className="btn-question btn-shadow" block bsSize="large" type="submit" title="Perguntar">
							<Link to="/app/pergunta/add" >FAÇA SUA PERGUNTA</Link>
						</Button>
					</Col>
				</Row>
			</div>
			<QuestionList handleAnswers={this.handleAnswers} onChangePage={this.onChangePage} page={this.page} />
            <div className="container pagination-flex-left">
				<Pagination 
					totalRecords={ this.props.total }
					pageLimit={ this.limit }
					pagesToShow={ parseInt(process.env.REACT_APP_GRID_PAGINATION_MAX_PAGES_TO_SHOW) }
					onChangePage={ this.onChangePage }
					page={ this.page } 
				/>
            </div>
			<Footer />
		</div>
    ); 
  }
}

const mapStateToProps = state => ({
	questionList: state.questionsPaginated.data,
	questionLoading: state.questionsPaginated.loading,
	questionError: state.questionsPaginated.error,
	total: state.questionsPaginated.total
});

const mapDispatchToProps = dispatch => 
    bindActionCreators(QuestionActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Question));