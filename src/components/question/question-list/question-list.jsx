import React from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Row, Col } from "react-bootstrap";
import * as moment from 'moment';
import * as Utils from '../../../commons/utils'

import './question-list.css'

const QuestionList = props => {

    const renderRows = () => {
        let questionsRows = props.listPaginated.map(q => (
			<Row className="summary">
				<Col sm={ 2 } className="count">
					<h4>Respostas: {q.answerTotal}</h4>
					<h4>Curtidas: {q.like}</h4>
				</Col>
				<Col sm={ 8 } className="summary-wrapper">
					<h2>
						<Link className="add-link" replace to="#" onClick={ () => { props.handleAnswers(q); } }>	
							{q.title}
						</Link>
					</h2>
					<div className="userinfo">
						<span className="relativetime" title={moment(q.createdIn).toDate().toLocaleDateString('pt-BR')}>
							{Utils.formatInterval(q.interval)}
						</span>
						<Link className="add-link" to="#">
							{q.userName}
						</Link>
					</div>
					<div className="tags">
						{ q.tags.map(t => (
							<Link className="add-link bubble" to="#">
								{t.tag}
							</Link>
						)) }
					</div>
				</Col>
			</Row>
        ));
    
        if (questionsRows.length === 0) 
			questionsRows.push(<h3>Nenhuma perguntada encontrada</h3>);
        
        return questionsRows
	}
	
	return (
		<div>
			<div className="questions">
				<div className="questions-header">
					<h1 className="subtitle">
						Todas as Perguntas
					</h1>
				</div>
				<div className="question-body">
					{ renderRows() }
				</div>
			</div>
		</div>
	)
};

const mapStateToProps = state => ({
	listPaginated: state.questionsPaginated.data
});

export default connect(mapStateToProps)(QuestionList);