import React, { Component } from 'react';
import './question-form.css';
import TagsSelect from '../../tags-select/tags-select'
import PropTypes from "prop-types";

export default class QuestionForm extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      tags: [],
      title: '',
      text: ''
    };
  }

  changeTitle = ( value ) => {
    this.setState(state => ({
      ...state,
      title: value
    }));
  };

  changeDescription = ( value ) => {
    this.setState(state => ({
      ...state,
      text: value
    }));
  };

  selectTags = (selected) => {
    this.setState(state => ({
      ...state,
      tags: selected && selected.length > 0 ? selected.map(item => item.value) : []
    }));
  };

  render() {
    return (
      <div>
        <div className="form-body container">
          <div className="row">
            <div className="form-control-row form-field-required col-md-1 offset-md-1">
              *
            </div>
            <div className="form-control-row col-md-8">
              <input 
                type="text" 
                autoFocus
                className="form-control" 
                placeholder="Ex: Como baixar um filme?"
                title="Título" 
                name="title" 
                value={ this.state.title }
                onChange={ e => { this.changeTitle(e.target.value) } }
              />
            </div>
          </div>
          <div className="row">
            <div className="form-control-row col-md-8 offset-md-2">
              <textarea 
                className="form-control" 
                placeholder="Caso seja necessário, adicione mais informações" 
                rows="8" 
                name="text" 
                maxLength="1000"
                value={ this.state.text }
                onChange={ e => { this.changeDescription(e.target.value) }} 
              />
              <span>Limit permitido 1000 caracteres</span> <br></br>
              <span>Total digitado: { this.state.text.length } </span>
            </div>
          </div>
          <div className="row">
            <div className="form-control-row col-md-8 offset-md-2">
                <TagsSelect 
                    multi={ true }
                    onChange={ (selectedList) => { this.setState(state => ({...this.state, tags: selectedList})) } } 
                />
            </div>
          </div>
        </div>
        <div className="form-footer d-flex">
          <div className="col-md-6">
            <button type="button" className="btn purple-background" onClick={ () => this.props.handleCancel() }>CANCELAR</button>
          </div>
          <div className="col-md-6 d-flex justify-content-end">
            <button type="button" className="btn purple-background justify-content-right" onClick={ () => this.props.handleSave(this.state) }>SALVAR</button>
          </div>
        </div>
      </div>
    );
  }
}

QuestionForm.propTypes = {
  handleCancel: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired
};