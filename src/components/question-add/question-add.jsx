import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestion } from '@fortawesome/free-solid-svg-icons';
import * as Utils from '../../commons/utils.js';
import { connect } from 'react-redux';
import { fetchQuestionAdd, FETCH_QUESTION_ADD_FAILURE } from '../../actions/question-add-actions';
import QuestionForm from './question-form/question-form'
import './question-add.css';


class QuestionAdd extends Component {

  handleCancel = () => {
    this.props.history.push('/home');
  };

  handleSave = (question) => {

    const questionToSave = { ...question };
 
    if (!questionToSave.title) {
      Utils.addNotification('O preenchimento do Título é obrigatório', Utils.NotificationType.WARNING);
      return;
    }else if (questionToSave.tags.length === 0){
      Utils.addNotification('Selecione no mínimo 1 tag para continuar', Utils.NotificationType.WARNING);
      return;
    }
    
    questionToSave.tags = questionToSave.tags.map(m => ({ tag_id: m.value }));
    
    this.props.fetchQuestionAdd(questionToSave).then((response) => {
      if (response.type === FETCH_QUESTION_ADD_FAILURE) throw response.payload;
      Utils.addNotification('Pergunta salva com sucesso!', Utils.NotificationType.SUCCESS);
      this.props.history.push('/home');
    }).catch((response) => {
        Utils.addNotification('Falha ao salvar o pergunta', Utils.NotificationType.DANGER);
    });
  };

  render() {
    return (
      <div>
        <div className="form-container">
          <div className="form-title d-flex justify-content-center">
            <h4><FontAwesomeIcon icon={ faQuestion } size="2x" /> FAÇA SUA PERGUNTA</h4>
          </div>
          <QuestionForm
            handleCancel={ this.handleCancel }
            handleSave={ this.handleSave } />
        </div>
      </div>

    );
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  fetchQuestionAdd: (question) => dispatch(fetchQuestionAdd(question))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(QuestionAdd));