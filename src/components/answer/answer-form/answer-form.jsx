import React, { Component } from 'react';
import './answer-form.css';
import PropTypes from "prop-types";

export default class AnswerForm extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  changeDescription = ( value ) => {
    this.setState(state => ({
      ...state,
      text: value
    }));
  };

  cleanDescription = () => {
    this.setState(state => ({
      ...state,
      text: ''
    }));
  }

  render() {
    return (
      <div>
        <div className="form-answer container">
          <div className="row">
            <div className="form-control-row form-field-required col-md-1 offset-md-1">
                *
            </div>
            <div className="form-control-row col-md-8 offset-md-2">
              <textarea 
                className="form-control" 
                rows="8" 
                name="text" 
                maxLength="1000"
                value={ this.state.text }
                onChange={ e => { this.changeDescription(e.target.value) }} 
              />
              <span>Limit permitido 1000 caracteres</span> <br></br>
              <span>Total digitado: { this.state.text.length } </span>
            </div>
          </div>
          <div className="row">
            <div className="form-control-row col-md-10 offset-md-2">
                <button style={{ marginLeft: "90px" }} type="button" className="btn purple-background" onClick={ () => { 
                  this.props.handleSave(this.state)
                  this.cleanDescription()
                } }>RESPONDER</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AnswerForm.propTypes = {
  handleSave: PropTypes.func.isRequired
};